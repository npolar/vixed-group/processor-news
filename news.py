#!/usr/bin/env python3

import feedparser
import newspaper
import argparse
import zipfile
import logging


def get_news(url_list, output_path, news_len=25):
    with open(output_path, mode='wb') as of:
        for url in url_list:
            feed = feedparser.parse(url)
            feed_name = feed.feed.title
            of.write(b'# ' + feed_name.encode() + b"\n")
            for n, i in enumerate(feed.entries):
                if n == news_len: break

                of.write(b'## '+i.title.encode() + b'\n\n')
                a = newspaper.Article(i.link)
                a.download()
                a.parse()
                of.write(a.text.encode('utf8'))
                of.write(b'\n--------\n')

def main():
    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    args = p.parse_args()
    global logger
    logger = logging.getLogger('newsmaker')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s  %(name)s  %(levelname)s: %(message)s')

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    url_list = [
            'https://www.nrk.no/toppsaker.rss',
            'https://www.nrk.no/sport/toppsaker.rss'
            ]
    get_news(url_list, args.output_file)

if __name__ == '__main__':
    main()
